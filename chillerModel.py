#import numpy as np
from array import array
from ROOT import *
import math
import collections
import pandas as pd

class chiller(object) :

    def __init__(self):
        self.setChiller("grant")

    #Julabo W80t power vs. temp
    powertemp_julabo = {
        100.0 : 1200.0 ,
         20.0 : 1200.0 ,
          0.0 : 1200.0 ,
        -20.0 : 1100.0 ,
        -40.0 : 1100.0 ,
        -60.0 :  650.0 ,
        -80.0 :  100.0 ,
        -85.0 :    0.0
        }

    #Huber CC-508 power vs. temp
    powertemp_huber = {
        100.0 : 1500.0 ,
         20.0 : 1500.0 ,
          0.0 : 1500.0 ,
        -20.0 : 1000.0 ,
        -40.0 :  300.0 ,
        -55.0 :   10.0 ,
       -100.0 :    0.0
        }

    # Grant R5 power vs. temp
    powertemp_grant = {
      1000.0 : 1100.0 ,
        20.0 : 1100.0 ,
         0.0 : 1050.0 ,
       -10.0 :  800.0 ,
       -20.0 :  580.0 ,
       -30.0 :  370.0 , #370.0 ,
       -40.0 :  130.0 , #130.0 ,
       -47.0 :   22.0 ,
     -1000.0 :    0.0 ,
    }

    # Grant R5 fitted from measured power
    powertemp_grant_fitted = {
      -1000.0 :    0.0 ,
        -43.0 :    0.0 ,
        -40.0 :   20.0 ,
        -20.0 :  380.0 ,
         -8.0 :  700.0 ,
          0.0 : 1020.0 ,
         10.0 : 1190.0 ,
         18.0 : 1200.0 ,
       1000.0 : 1200.0 , 
    }

    def setChiller(self,thechiller):
        if thechiller == "huber":
            powertemp = self.powertemp_huber
        elif thechiller == "julabo":
            powertemp = self.powertemp_julabo
        elif thechiller == "grant":
            powertemp = self.powertemp_grant_fitted
        else:
            print "Chiller not recognised. Doing nothing."

        powertemp = collections.OrderedDict(sorted(powertemp.items()))
        temps = powertemp.keys()
        powers = powertemp.values()
        
        x = array("d",temps)
        y = array("d",powers)
        self.powergraph = TGraph(len(x),x,y)
    
    def power(self, temp) :
        chillpower = self.powergraph.Eval(temp)
        return chillpower * -1.0




class coldboxsim(object):

    # Heat removed on cold side at 1.5A
    peltier_heat_removed = {
       -20.0 : 16.0 ,
         0.0 : 16.0 ,
        20.0 :  9.0 ,
        40.0 :  0.0 ,
        60.0 : -9.0
    }

    # Heat added to hot side at 1.5A
    peltier_heat_waste = {
       -20.0 : 24.0 ,
         0.0 : 24.0 ,
        20.0 : 18.0 ,
        40.0 :  8.0 ,
        60.0 :  4.0 
    }

    # Define power graphs of a single peltier
    heat_removed = collections.OrderedDict(sorted(peltier_heat_removed.items()))
    temps = heat_removed.keys()
    powers = heat_removed.values()
    x = array("d",temps)
    y = array("d",powers)
    peltier_cooling_graph = TGraph(len(x),x,y)

    heat_waste = collections.OrderedDict(sorted(peltier_heat_waste.items()))
    temps = heat_waste.keys()
    powers = heat_waste.values()
    x = array("d",temps)
    y = array("d",powers)
    peltier_heatwaste_graph = TGraph(len(x),x,y)


    def setChiller(self,newchiller):
        self.thechiller.setChiller(newchiller)

    def setChill(self,chill):
        self.chill = bool(chill)

    def setPeltierOn(self,peltierOn):
        self.peltierOn = bool(peltierOn)

    def setDuration(self,duration):
        self.duration = duration

    def setTempStart(self,T):
        self.tempStart = T

    def setTempEnv(self,T):
        self.tempEnv = T

    def setTempN2(self,T):
        self.tempN2 = T

    def setTempCoolantStart(self,T):
        self.tempCoolantStart = T

    def setTempCoolantConst(self,const):
        self.tempCoolantConst = bool(const)

    def setConductanceBox(self,c):
        self.C_box = c

    def setConductanceTubes(self,c):
        self.C_tubes = c

    def setHeatTransferCB_CO(self,H):
        self.H_cb_co = H

    def setHeatTransferCB_VC(self,H):
        self.H_cb_vc = H

    def setHeatTransferCB_PI(self,H):
        self.H_cb_pi = H    

    def setHeatTransferCB_SB(self,H):
        self.H_cb_sb = H

    def setHeatTransferPI_CO(self,H):
        self.H_pi_co = H

    def setHeatTransferPI_SB(self,H):
        self.H_pi_sb = H

    def setHeatTransferPI_N2(self,H):
        self.H_pi_n2 = H

    def setHeatTransferSB_SL(self,H):
        self.H_sb_sl = H

    def setHeatTransferSB_N2(self,H):
        self.H_sb_n2 = H

    def setHeatTransferVC_N2(self,H):
        self.H_vc_n2 = H

    def setHeatCapacityBP(self,alpha):
        self.alpha_bp = alpha

    def setHeatCapacityCB(self,alpha):
        self.alpha_cb = alpha

    def setHeatCapacityVC(self,alpha):
        self.alpha_vc = alpha

    def setHeatCapacitySB(self,alpha):
        self.alpha_sb = alpha

    def setHeatCapacitySL(self,alpha):
        self.alpha_sl = alpha

    def setHeatCapacityPI(self,alpha):
        self.alpha_pi = alpha

    def setVolumeCoolant(self,V):
        self.V_cool = V

    # Heat capacity of coolant vs. temperature
    # From http://detector-cooling.web.cern.ch/Detector-Cooling/data/Table%208-3-1.htm
    tempHC = {
        -40.0 : 3040.0 ,
        -20.0 : 3110.0 ,
          0.0 : 3190.0 ,
         20.0 : 3260.0 ,
         40.0 : 3340.0 ,
    }

    temps = tempHC.keys()
    temps = [-100.] + temps + [100.]
    HCs = tempHC.values()
    HCs = [HCs[0]] + HCs + [HCs[-1]]
    x = array("d",temps)
    y = array("d",HCs)
    HCgraph = TGraph(len(x),x,y)

    def coolant_alpha(self, mass, temp):
        alpha = mass*self.HCgraph.Eval(temp)
        return alpha

    dt = 0.1 # Time increment in seconds
    C_box = 0. # Conductance of box insulation [W/K]
    C_tubes = 0. # Additional conductance of the pipes etc. outside the box that heats the coolant

    # Heat transfer coefficients [W/K]
    H_cb_co = 0. # Cold blocks <-> coolant
    H_cb_vc = 0. # Cold blocks <-> vacuum chucks
    H_cb_pi = 0. # Cold blocks <-> copper pipes
    H_cb_sb = 0. # Cold blocks <-> steel box
    H_pi_co = 0. # Pipes <-> coolant
    H_pi_sb = 0. # Copper pipes <-> steel box
    H_pi_n2 = 0. # Copper pipes <-> nitrogen
    H_sb_sl = 0. # Steel box <-> steel lid
    H_sb_n2 = 0. # Steel box <-> nitrogen
    H_vc_n2 = 0. # Vacuum chucks <-> nitrogen

    alpha_bp = 0.
    alpha_cb = 0.
    alpha_vc = 0.
    alpha_sb = 0.
    alpha_sl = 0.
    alpha_pi = 0.
    V_cool = 0.

    tempStart = 25.
    tempEnv = 22.
    tempCoolantStart = -40.
    tempN2 = -20.
    duration = 3600.
    chill = False
    peltierOn = False
    tempCoolantConst = False

    thechiller = chiller()

    # Duration in minutes
    def runSim(self):

        self.m_cool = self.V_cool * (0.6*1.11 + 0.4*1.0)

        self.tempCoolant = self.tempCoolantStart
        self.tempCB = self.tempStart
        self.tempVC = self.tempStart
        self.tempPipes = self.tempStart
        self.tempSteelbox = self.tempStart
        self.tempSteellid = self.tempStart

        self.alpha_cbtot = self.alpha_bp + self.alpha_cb # Heat capacity of cold block and base plate

        self.use_vc = True if self.alpha_vc > 0. else False
        self.use_sb = True if self.alpha_sb > 0. else False
        self.use_sl = True if self.alpha_sl > 0. else False
        self.use_pi = True if self.alpha_pi > 0. else False

        time = [x*self.dt for x in range(int(self.duration/self.dt))]
        tempvecCoolant = [None] * len(time)
        tempvecCB = [None] * len(time)
        tempvecVC = [None] * len(time)
        tempvecPipes = [None] * len(time)
        tempvecSteelbox = [None] * len(time)
        tempvecSteellid = [None] * len(time)

        if not type(self.tempEnv) is list:
            print "tempEnv is not a list. Creating list with constant values now"
            self.tempEnv = [self.tempEnv for i in time]

        if not type(self.tempN2) is list:
            print "tempN2 is not a list. Creating list with constant values now"
            self.tempN2 = [self.tempN2 for i in time]

        print "Running Sim"
        tempEnvtimestep = self.duration/float(len(self.tempEnv))
        tempN2timestep = self.duration/float(len(self.tempN2))

        t = 0
        for tNow in time:

            tempentry = int(tNow/tempEnvtimestep)
            if tempentry > len(self.tempEnv)-1:
                tempentry = len(self.tempEnv)-1
            tempEnvNow = self.tempEnv[tempentry]

            tempentry = int(tNow/tempN2timestep)
            if tempentry > len(self.tempN2)-1:
                tempentry = len(self.tempN2)-1
            tempN2Now = self.tempN2[tempentry]

            tempvecCoolant[t] = self.tempCoolant
            tempvecCB[t] = self.tempCB
            tempvecVC[t] = self.tempVC
            tempvecPipes[t] = self.tempPipes
            tempvecSteelbox[t] = self.tempSteelbox
            tempvecSteellid[t] = self.tempSteellid

            power_coolant =   int(self.chill) * self.thechiller.power(self.tempCoolant) \
                            + self.C_tubes * (tempEnvNow - self.tempCoolant) \
                            + int(self.use_pi) * self.H_pi_co * (self.tempPipes - self.tempCoolant) \
                            + self.H_cb_co * (self.tempCB - self.tempCoolant) \

            power_cb =   self.H_cb_co * (self.tempCoolant - self.tempCB) \
                       + int(self.use_sb) * self.H_cb_sb * (self.tempSteelbox - self.tempCB) \
                       + int(self.use_pi) * self.H_cb_pi * (self.tempPipes - self.tempCB) \
                       + int(self.use_vc) * self.H_cb_vc * (self.tempVC - self.tempCB) \
                       + int(self.peltierOn) * self.peltier_heatwaste_graph.Eval(self.tempCB - self.tempVC) * 16.

            if self.use_vc:
                power_vc =   self.H_cb_vc * (self.tempCB - self.tempVC) \
                           + self.H_vc_n2 * (tempN2Now - self.tempVC) \
                           - int(self.peltierOn) * self.peltier_cooling_graph.Eval(self.tempCB - self.tempVC) * 16.
            else:
                power_vc = 0.

            if self.use_pi:
                power_pipes =    self.H_pi_co * (self.tempCoolant - self.tempPipes) \
                               + int(self.use_sb) * self.H_pi_sb * (self.tempSteelbox - self.tempPipes) \
                               + self.H_pi_n2 * (22. - self.tempPipes) \
                               + self.H_cb_pi * (self.tempCB - self.tempPipes)
            else:
                power_pipes = 0.

            if self.use_sb:
                power_steelbox =   self.H_cb_sb * (self.tempCB - self.tempSteelbox) \
                                 + int(self.use_pi) * self.H_pi_sb * (self.tempPipes - self.tempSteelbox) \
                                 + self.H_sb_n2 * (tempN2Now - self.tempSteelbox) \
                                 + self.C_box*(tempEnvNow - self.tempSteelbox) \
                                 + int(self.use_sl) * self.H_sb_sl * (self.tempSteellid - self.tempSteelbox)
            else:
                power_steelbox = 0.

            if self.use_sl:
                power_steellid = int(self.use_sb) * self.H_sb_sl * (self.tempSteelbox - self.tempSteellid)
            else:
                power_steellid = 0.

            deltaTemp_coolant = min( 5.*self.dt, (power_coolant / self.coolant_alpha(self.m_cool,self.tempCoolant)) * self.dt )
            deltaTemp_cb = min( 5.*self.dt, (power_cb / self.alpha_cbtot) * self.dt ) if self.alpha_cbtot != 0. else 0.
            deltaTemp_vc = min( 5.*self.dt, (power_vc / self.alpha_vc) * self.dt ) if self.alpha_vc != 0. else 0.
            deltaTemp_pipes = min( 5.*self.dt, (power_pipes / self.alpha_pi) * self.dt ) if self.alpha_pi != 0. else 0.
            deltaTemp_steelbox = min( 5.*self.dt, (power_steelbox / self.alpha_sb) * self.dt ) if self.alpha_sb != 0. else 0.
            deltaTemp_steellid = min( 5.*self.dt, (power_steellid / self.alpha_sl) * self.dt ) if self.alpha_sl != 0. else 0.

            self.tempCoolant += deltaTemp_coolant if not self.tempCoolantConst else 0. 
            self.tempCB += deltaTemp_cb
            self.tempVC += deltaTemp_vc
            self.tempPipes += deltaTemp_pipes
            self.tempSteelbox += deltaTemp_steelbox
            self.tempSteellid += deltaTemp_steellid

            t += 1


        output = [
            ("Time",time),
            ("Meas T",tempvecCoolant),
            ("CB",tempvecCB),
            ("VC",tempvecVC),
            ("BOX",tempvecSteelbox),
            ("Pipes",tempvecPipes),
            ("LID",tempvecSteellid)
        ]
        output = pd.DataFrame.from_items(output)
        output["Elapsed_hr"] = output["Time"] / 3600.0
        output.reset_index(inplace=True)
        output.set_index(output["Elapsed_hr"], inplace=True)

        return output

        print ".....done"
