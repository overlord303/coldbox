import glob
import matplotlib.pyplot as plt

# Files to analyse

# -- Long 2-bath test
#/Volumes/TEAM1/T77/ATLAS/Logs/Log170601123312.csv

# -- Peltier Test
#/Volumes/TEAM1/T77/ATLAS/Logs/Log170602103149.csv

# Thermal Scan
#/Volumes/TEAM1/T77/ATLAS/Logs/Log170522102701.csv
#/Volumes/TEAM1/T77/ATLAS/Logs/Log170518101312.csv


import pandas as pd
import seaborn as sns

def loadData(filename) :

    print "processing", filename
    
    # Load CSV file
    data = pd.read_csv(filename,skiprows=2,
                   parse_dates=True,
                   dayfirst=True,
                   infer_datetime_format=True,
                   index_col=0)

    # Find columns with raw values
    rawValues = [colname
                     for colname in data.columns.values
                     if colname.find("_raw") != -1]

    # Find columns called 'Col 1', 'Col 2', etc
    colValues = [ colname
                      for colname in data.columns.values
                      if colname.find("Col ") != -1]

    # Find columns that are unnamed
    unnameValues =  [ colname
                      for colname in data.columns.values
                      if colname.find("Unnamed: ") != -1]


    drop_columns = rawValues + colValues + unnameValues

    # Drop values in drop_columns
    data.drop(drop_columns, axis=1, inplace=True)

    # Find first time Set T goes to -30
    # tZero = (data[data["Set T"] < 0.0])["Elapsed"].iloc[0]

    # Now make new Elapsed time that uses tZero to define 0.0 time and
    # have time in hours
    #data["Elapsed_hr"] = (data["Elapsed"] - tZero) / 3600.0
    
    return data
    
    


if __name__ == "__main__" :

    # # Standard thermal cycle
    # data1 = loadData("/Volumes/TEAM1/T77/ATLAS/Logs/Log170518101312.csv")
    # data2 = loadData("/Volumes/TEAM1/T77/ATLAS/Logs/Log170522102701.csv")    

    # # Two bath & chuck test
    # data3 = loadData("/Volumes/TEAM1/T77/ATLAS/Logs/Log170601123312.csv")

    # # Insulation cold test 
    # data4 = loadData("/Volumes/TEAM1/T77/ATLAS/Logs/Log170531163917.csv")

    # # Peltier test
    # data5 = loadData("/Volumes/TEAM1/T77/ATLAS/Logs/Log170602103149.csv")

    # # Chuck test 2
    # data6 = loadData("/Volumes/TEAM1/T77/ATLAS/Logs/Log170607152428.csv")

    # # First 2-bath test
    # data7 = loadData("/Volumes/TEAM1/T77/ATLAS/Logs/Log170407095232.csv")

    # # Humidity Reduction
    # data8 = loadData("/Volumes/TEAM1/T77/ATLAS/Logs/Log170616102730.csv")

    # # Cu Pipe Insulation Test
    # data9 = loadData("/Volumes/TEAM1/T77/ATLAS/Logs/Log170616121727.csv")

    # # Fast 2-bath switching
    # # NB: Very hot day  Outside temp 30C
    # data10 = loadData("/Volumes/TEAM1/T77/ATLAS/Logs/Log170619142537.csv")

    # # Fast 2-bath switching
    # # NB: Lab at normal temperature
    # data11 = loadData("/Volumes/TEAM1/T77/ATLAS/Logs/Log170821163243.csv")

    # # Cold-Jig warm up from -35C to room temperature
    # data12 = loadData("/Volumes/TEAM1/T77/ATLAS/Logs/Log170821202145.csv")

    # # Alternative "fast" 2-bath switch and cooling inlet bulkheads removed
    # data13 = loadData("/Volumes/TEAM1/T77/ATLAS/Logs/Log170915100306.csv")

    # # Chiller without coldbox
    # data14 = loadData("/Volumes/TEAM1/T77/ATLAS/Logs/Log170327172329.csv")

    # # Single Chiller, no bulkheads
    # data15 = loadData("/Volumes/TEAM1/T77/ATLAS/Logs/Log171003085924.csv")



    data1 = loadData("logs/Log171124145421.csv") # 30 --> -40C and Peltiers at 25V
    data2 = loadData("logs/Log171205133749.csv") # 30 --> -40C and Peltiers at 30V
    data3 = loadData("logs/Log171128153545.csv") # 30 --> -40C and Peltiers off
    data4 = loadData("logs/Log171129151835.csv") # 30 --> -40C and Peltiers at 25V and base plates isolated from inner box
    data5 = loadData("logs/Log171003085924.csv") # ?? --> -40C and Peltiers off and valves in place
    data6 = loadData("logs/Log171214111206.csv") # 30 --> -40V and peltiers at 25V and base plates isolated from inner box + N2 flow at 2L/min
    data7 = loadData("logs/Log180116130719.csv") # 30 --> -40C and Peltiers at 25V and shortened tubes from chiller
    data8 = loadData("logs/Log180216105506.csv") # Thermal shock -30C through single chill stack in plastic box. Two layers of peltiers at 25V (One broke during)
    data9 = loadData("logs/Log180222141240.csv") # One chill stack in plastic box. Chiller circulating at -20C. Two peltier layers. Layer 1 at 25V, then layer 2 at 3,6,9,12,15,18V
    
    data10 = loadData("logs/Log180226105338.csv") # Thermal shock -25C through single chill stack in plastic box. Pump speed 5. No HV insert.
    data11 = loadData("logs/Log180226121232.csv") # Thermal shock -25C through single chill stack in plastic box. Pump speed 3. No HV insert.
    data12 = loadData("logs/Log180226132932.csv") # Thermal shock -25C through single chill stack in plastic box. Pump speed 1. No HV insert.
    
    data13 = loadData("logs/Log180319135347.csv") # Single chill stack with new ground shim plate, cooldown chiller + peltiers at 25V from 20C 
    data14 = loadData("logs/Log180319153524.csv") # Single chill stack with new ground shim plate, coolant held at -20C, then turn peltiers on at 25V
    data15 = loadData("logs/Log180320104151.csv") # Single chill stack - old ground shim, cooldown chiller + peltiers at 25V from 20C 
    data16 = loadData("logs/Log180320121537.csv") # Single chill stack - old ground shim, coolant held at -20C, then turn peltiers on at 25V
    data17 = loadData("logs/Log180320130432.csv") # Single chill stack - heatup from cold with Peltiers in reverse at 25V
    
    data18 = loadData("logs/Log180322141606.csv") # Cold jig cooldown - old ground shim, chiller + Peltier (25V) cooldown from 30C
    data19 = loadData("logs/Log180323133301.csv") # Cold jig cooldown - new ground shim plate, chiller + Peltier (25V) cooldown from 30C
    data20 = loadData("logs/Log180326102928.csv") # Cold jig cooldown - only bottom alu plate, chiller + Peltier (25V) cooldown from 30C
    data21 = loadData("logs/Log180326152127.csv") # Cold jig cooldown - old ground shim, coolant starting at -5C and Peltiers in reverse to reach 30C on VCs. Then full cycle at 25V, 30C to -40C to 30C
    data22 = loadData("logs/Log180327113301.csv") # Cold jig cooldown - old ground shim, chiller + Peltier (25V) cooldown from 30C, 10mm tubes from chiller instead of 16mm
    data23 = loadData("logs/Log180327153927.csv") # Cold jig cooldown - old ground shim, coolant starting at -10C and Peltiers in reverse to reach 30C on VCs. Then full cycle at 30V, 30C to -40C to 30C
 
    data24 = loadData("logs/Log180403112843.csv") # Thermal shock of cold block in plastic box. Start at ~40C and coolant at -15C. Pump speed 1.
    data25 = loadData("logs/Log180404132713.csv") # Thermal shock of cold block in plastic box. Start at ~40C and coolant at -15C. Pump speed 2.
    data26 = loadData("logs/Log180404114704.csv") # Thermal shock of cold block in plastic box. Start at ~40C and coolant at -15C. Pump speed 3.
    data27 = loadData("logs/Log180405140416.csv") # Thermal shock of cold block in plastic box. Start at ~40C and coolant at -15C. Pump speed 4.
    data28 = loadData("logs/Log180329113313.csv") # Thermal shock of cold block in plastic box. Start at ~40C and coolant at -15C. Pump speed 5.

    data29 = loadData("logs/Log180409114252.csv") # Rapid Peltier cycling at 30V with coolant at -15C. Pump speed 5.
    data30 = loadData("logs/Log180409131734.csv") # Rapid Peltier cycling at 30V with coolant at -25C. Pump speed 5.
    data31 = loadData("logs/Log180409135616.csv") # Rapid Peltier cycling at 30V with coolant at -30C. Pump speed 5.
    data32 = loadData("logs/Log180409143122.csv") # Rapid Peltier cycling at 40V with coolant at -30C. Pump speed 5.
    data33 = loadData("logs/Log180409150636.csv") # Rapid Peltier cycling at 30V with coolant at -30C. Pump speed 1.
    data34 = loadData("logs/Log180410114354.csv") # Rapid Peltier cycling at 25V with coolant at -30C. Pump speed 5.

    data35 = loadData("logs/Log180411151227.csv") # Rapid Peltier cycling at 30V with coolant at -30C. New ground shim. Pump speed 5.
    data36 = loadData("logs/Log180412134444.csv") # Rapid Peltier cycling at 40V using graphite film with coolant at -30C. Pump speed 5.
    data37 = loadData("logs/Log180412140832.csv") # Rapid Peltier cycling at 30V using graphite film with coolant at -30C. Pump speed 5.
    data38 = loadData("logs/Log180412143033.csv") # Rapid Peltier cycling at 30V using graphite film with coolant at -30C. Pump speed 5 during cooldown, speed 1 during heatup.
    data39 = loadData("logs/Log180413111407.csv") # Rapid Peltier cycling at 30V using graphite film with coolant at -30C. Graphite above kapton film removed. Pump speed 5.
    data40 = loadData("logs/Log180413132502.csv") # Rapid Peltier cycling at 40V using graphite film with coolant at -30C. Graphite above kapton film removed. Pump speed 5.
    data41 = loadData("logs/Log180413130449.csv") # Rapid Peltier cycling at 40V using graphite film with coolant at -33C. Graphite above kapton film removed. Pump speed 5.
    data42 = loadData("logs/Log180413151921.csv") # Rapid Peltier cycling at 30V using graphite film with coolant at -30C. Only new ground plate (no vacuum chuck).
    data43 = loadData("logs/Log180416142807.csv") # Rapid Peltier cycling at 30V using graphite film with coolant at -30C. Only new ground plate (no vaccum chuck, better contact, thermocouple in middle)
    data44 = loadData("logs/Log180416144222.csv") # Rapid Peltier cycling at 30V using graphite film with coolant at -30C. Only new ground plate (no vaccum chuck, better contact, thermocouple near edge)
    data45 = loadData("logs/Log180418133249.csv") # Rapid Peltier cycling at 30V using graphite film with coolant at -30C. New ground shim. Pump speed 5.

    data46 = loadData("logs/Log180529114320.csv") # Peltier cycling test with aluminium chill stacks

    data47 = loadData("logs/Log180612154326.csv") # Two layers of Peltiers in plastic box cycle from +60C to -55C. Bottom 19V, top 6.8V. Huber chiller at -20C.
    data48 = loadData("logs/Log180619112904.csv") # Two layers of Peltiers in plastic box cycle from +60C to -55C. Bottom 19V, top 6.8V. Grant chiller at -20C.

    data49 = loadData("logs/Log180621133830.csv") # Two layers of Peltiers (one chill stack only) in 4-module jig. Coolant at -20C. Cycle +60C to -55C. Bottom 19V, top 7V. Huber chiller.
    data50 = loadData("logs/Log180621152558.csv") # Two layers of Peltiers (one chill stack only) in 4-module jig. Coolant at -20C. Cycle +60C to -55C. Bottom 19V, top 7V. Huber chiller. Extra insulation.
    data51 = loadData("logs/Log180621160411.csv") # Two layers of Peltiers (one chill stack only) in 4-module jig. Coolant at -20C. Cycle +60C to -55C. Bottom 18V, top 6.6V. Huber chiller. Extra insulation.
    data52 = loadData("logs/Log180621164814.csv") # Two layers of Peltiers (one chill stack only) in 4-module jig. Coolant at -25C. Cycle +60C to -55C. Bottom 18V, top 6.6V. Huber chiller. Extra insulation.

    data1_t0 = 0.
    data2_t0 = 0.
    data3_t0 = 0.
    data4_t0 = 0.
    data5_t0 = 0.
    data6_t0 = 0.
    data7_t0 = 0.
    data8_t0 = 0.
    data9_t0 = 0.
    data10_t0 = 5.
    data11_t0 = 0.
    data12_t0 = 5.
    data13_t0 = 5.
    data14_t0 = 5.
    data15_t0 = 5.
    data16_t0 = 5.
    data17_t0 = 5.
    data18_t0 = 5.
    data19_t0 = 60.
    data20_t0 = 5.
    data21_t0 = 5.
    data22_t0 = 5.
    data23_t0 = 25.4*60.
    data24_t0 = 12.
    data25_t0 = 35.
    data26_t0 = 68.
    data27_t0 = 10.
    data28_t0 = 0.
    data29_t0 = 0.
    data30_t0 = 0.
    data31_t0 = 13.
    data32_t0 = 0.
    data33_t0 = 0.
    data34_t0 = 0.
    data35_t0 = 15.
    data36_t0 = 0.
    data37_t0 = 5.
    data38_t0 = 0.
    data39_t0 = 11.
    data40_t0 = 0.
    data41_t0 = 7.
    data42_t0 = 0.
    data43_t0 = 0.
    data44_t0 = 5.
    data45_t0 = 0.
    data46_t0 = 0.
    data47_t0 = 0.
    data48_t0 = 0.
    data49_t0 = 0.
    data50_t0 = 0.
    data51_t0 = 0.
    data52_t0 = 0.





    # Apply t0 corrections and convert from seconds to hours

    data1["Elapsed_hr"] = (data1["Elapsed"] - data1_t0) / 3600.0
    data2["Elapsed_hr"] = (data2["Elapsed"] - data2_t0) / 3600.0
    data3["Elapsed_hr"] = (data3["Elapsed"] - data3_t0) / 3600.0
    data4["Elapsed_hr"] = (data4["Elapsed"] - data4_t0) / 3600.0
    data5["Elapsed_hr"] = (data5["Elapsed"] - data5_t0) / 3600.0
    data6["Elapsed_hr"] = (data6["Elapsed"] - data6_t0) / 3600.0
    data7["Elapsed_hr"] = (data7["Elapsed"] - data7_t0) / 3600.0
    data8["Elapsed_hr"] = (data8["Elapsed"] - data8_t0) / 60.
    data9["Elapsed_hr"] = (data9["Elapsed"] - data9_t0) / 3600.0
    data10["Elapsed_hr"] = (data10["Elapsed"] - data10_t0) / 60.
    data11["Elapsed_hr"] = (data11["Elapsed"] - data11_t0) / 60.
    data12["Elapsed_hr"] = (data12["Elapsed"] - data12_t0) / 60.
    data13["Elapsed_hr"] = (data13["Elapsed"] - data13_t0) / 60.
    data14["Elapsed_hr"] = (data14["Elapsed"] - data14_t0) / 60.
    data15["Elapsed_hr"] = (data15["Elapsed"] - data15_t0) / 60.
    data16["Elapsed_hr"] = (data16["Elapsed"] - data16_t0) / 60.
    data17["Elapsed_hr"] = (data17["Elapsed"] - data17_t0) / 60.
    data18["Elapsed_hr"] = (data18["Elapsed"] - data18_t0) / 60.
    data19["Elapsed_hr"] = (data19["Elapsed"] - data19_t0) / 60.
    data20["Elapsed_hr"] = (data20["Elapsed"] - data20_t0) / 60.
    data21["Elapsed_hr"] = (data21["Elapsed"] - data21_t0) / 60.
    data22["Elapsed_hr"] = (data22["Elapsed"] - data22_t0) / 60.
    data23["Elapsed_hr"] = (data23["Elapsed"] - data23_t0) / 60.
    data24["Elapsed_hr"] = (data24["Elapsed"] - data24_t0) / 60.
    data25["Elapsed_hr"] = (data25["Elapsed"] - data25_t0) / 60.
    data26["Elapsed_hr"] = (data26["Elapsed"] - data26_t0) / 60.
    data27["Elapsed_hr"] = (data27["Elapsed"] - data27_t0) / 60.
    data28["Elapsed_hr"] = (data28["Elapsed"] - data28_t0) / 60.
    data29["Elapsed_hr"] = (data29["Elapsed"] - data29_t0) / 60.
    data30["Elapsed_hr"] = (data30["Elapsed"] - data30_t0) / 60.
    data31["Elapsed_hr"] = (data31["Elapsed"] - data31_t0) / 60.
    data32["Elapsed_hr"] = (data32["Elapsed"] - data32_t0) / 60.
    data33["Elapsed_hr"] = (data33["Elapsed"] - data33_t0) / 60.
    data34["Elapsed_hr"] = (data34["Elapsed"] - data34_t0) / 60.
    data35["Elapsed_hr"] = (data35["Elapsed"] - data35_t0) / 60.
    data36["Elapsed_hr"] = (data36["Elapsed"] - data36_t0) / 60.
    data37["Elapsed_hr"] = (data37["Elapsed"] - data37_t0) / 60.
    data38["Elapsed_hr"] = (data38["Elapsed"] - data38_t0) / 60.
    data39["Elapsed_hr"] = (data39["Elapsed"] - data39_t0) / 60.
    data40["Elapsed_hr"] = (data40["Elapsed"] - data40_t0) / 60.
    data41["Elapsed_hr"] = (data41["Elapsed"] - data41_t0) / 60.
    data42["Elapsed_hr"] = (data42["Elapsed"] - data42_t0) / 60.
    data43["Elapsed_hr"] = (data43["Elapsed"] - data43_t0) / 60.
    data44["Elapsed_hr"] = (data44["Elapsed"] - data44_t0) / 60.
    data45["Elapsed_hr"] = (data45["Elapsed"] - data45_t0) / 60.
    data46["Elapsed_hr"] = (data46["Elapsed"] - data46_t0) / 3600.
    data47["Elapsed_hr"] = (data47["Elapsed"] - data47_t0) / 60.
    data48["Elapsed_hr"] = (data48["Elapsed"] - data48_t0) / 60.
    data49["Elapsed_hr"] = (data49["Elapsed"] - data49_t0) / 60.
    data50["Elapsed_hr"] = (data50["Elapsed"] - data50_t0) / 60.
    data51["Elapsed_hr"] = (data51["Elapsed"] - data51_t0) / 60.
    data52["Elapsed_hr"] = (data52["Elapsed"] - data52_t0) / 60.

    data1.reset_index(inplace=True)
    data2.reset_index(inplace=True)
    data3.reset_index(inplace=True)
    data4.reset_index(inplace=True)
    data5.reset_index(inplace=True)
    data6.reset_index(inplace=True)
    data7.reset_index(inplace=True)
    data8.reset_index(inplace=True)
    data9.reset_index(inplace=True)
    data10.reset_index(inplace=True)
    data11.reset_index(inplace=True)
    data12.reset_index(inplace=True)
    data13.reset_index(inplace=True)
    data14.reset_index(inplace=True)
    data15.reset_index(inplace=True)
    data16.reset_index(inplace=True)
    data17.reset_index(inplace=True)
    data18.reset_index(inplace=True)
    data19.reset_index(inplace=True)
    data20.reset_index(inplace=True)
    data21.reset_index(inplace=True)
    data22.reset_index(inplace=True)
    data23.reset_index(inplace=True)
    data24.reset_index(inplace=True)
    data25.reset_index(inplace=True)
    data26.reset_index(inplace=True)
    data27.reset_index(inplace=True)
    data28.reset_index(inplace=True)
    data29.reset_index(inplace=True)
    data30.reset_index(inplace=True)
    data31.reset_index(inplace=True)
    data32.reset_index(inplace=True)
    data33.reset_index(inplace=True)
    data34.reset_index(inplace=True)
    data35.reset_index(inplace=True)
    data36.reset_index(inplace=True)
    data37.reset_index(inplace=True)
    data38.reset_index(inplace=True)
    data39.reset_index(inplace=True)
    data40.reset_index(inplace=True)
    data41.reset_index(inplace=True)
    data42.reset_index(inplace=True)
    data43.reset_index(inplace=True)
    data44.reset_index(inplace=True)
    data45.reset_index(inplace=True)
    data46.reset_index(inplace=True)
    data47.reset_index(inplace=True)
    data48.reset_index(inplace=True)
    data49.reset_index(inplace=True)
    data50.reset_index(inplace=True)
    data51.reset_index(inplace=True)
    data52.reset_index(inplace=True)

    data1.set_index(data1["Elapsed_hr"], inplace=True)    
    data2.set_index(data2["Elapsed_hr"], inplace=True)
    data3.set_index(data3["Elapsed_hr"], inplace=True)
    data4.set_index(data4["Elapsed_hr"], inplace=True)
    data5.set_index(data5["Elapsed_hr"], inplace=True)
    data6.set_index(data6["Elapsed_hr"], inplace=True)        
    data7.set_index(data7["Elapsed_hr"], inplace=True)
    data8.set_index(data8["Elapsed_hr"], inplace=True)
    data9.set_index(data9["Elapsed_hr"], inplace=True)
    data10.set_index(data10["Elapsed_hr"], inplace=True)
    data11.set_index(data11["Elapsed_hr"], inplace=True)
    data12.set_index(data12["Elapsed_hr"], inplace=True)
    data13.set_index(data13["Elapsed_hr"], inplace=True)
    data14.set_index(data14["Elapsed_hr"], inplace=True)
    data15.set_index(data15["Elapsed_hr"], inplace=True)
    data16.set_index(data16["Elapsed_hr"], inplace=True)
    data17.set_index(data17["Elapsed_hr"], inplace=True)
    data18.set_index(data18["Elapsed_hr"], inplace=True)
    data19.set_index(data19["Elapsed_hr"], inplace=True)
    data20.set_index(data20["Elapsed_hr"], inplace=True)
    data21.set_index(data21["Elapsed_hr"], inplace=True)
    data22.set_index(data22["Elapsed_hr"], inplace=True)
    data23.set_index(data23["Elapsed_hr"], inplace=True)
    data24.set_index(data24["Elapsed_hr"], inplace=True)
    data25.set_index(data25["Elapsed_hr"], inplace=True)
    data26.set_index(data26["Elapsed_hr"], inplace=True)
    data27.set_index(data27["Elapsed_hr"], inplace=True)
    data28.set_index(data28["Elapsed_hr"], inplace=True)
    data29.set_index(data29["Elapsed_hr"], inplace=True)
    data30.set_index(data30["Elapsed_hr"], inplace=True)
    data31.set_index(data31["Elapsed_hr"], inplace=True)
    data32.set_index(data32["Elapsed_hr"], inplace=True)
    data33.set_index(data33["Elapsed_hr"], inplace=True)
    data34.set_index(data34["Elapsed_hr"], inplace=True)
    data35.set_index(data35["Elapsed_hr"], inplace=True)
    data36.set_index(data36["Elapsed_hr"], inplace=True)
    data37.set_index(data37["Elapsed_hr"], inplace=True)
    data38.set_index(data38["Elapsed_hr"], inplace=True)
    data39.set_index(data39["Elapsed_hr"], inplace=True)
    data40.set_index(data40["Elapsed_hr"], inplace=True)
    data41.set_index(data41["Elapsed_hr"], inplace=True)
    data42.set_index(data42["Elapsed_hr"], inplace=True)
    data43.set_index(data43["Elapsed_hr"], inplace=True)
    data44.set_index(data44["Elapsed_hr"], inplace=True)
    data45.set_index(data45["Elapsed_hr"], inplace=True)
    data46.set_index(data46["Elapsed_hr"], inplace=True)
    data47.set_index(data47["Elapsed_hr"], inplace=True)    
    data48.set_index(data48["Elapsed_hr"], inplace=True)  
    data49.set_index(data49["Elapsed_hr"], inplace=True)  
    data50.set_index(data50["Elapsed_hr"], inplace=True)  
    data51.set_index(data51["Elapsed_hr"], inplace=True)
    data52.set_index(data52["Elapsed_hr"], inplace=True)


    #===============
    # Plotting
    #===============

    # Plot standard run +30C --> -40C
    # ax = data1[["VC1","VC4","Meas T"]].plot(color=["black","green","blue"],style="-",grid=True,title="",figsize=(10,7))
    # ax.set_xlabel("Time [h]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,2.7)
    # ax.legend(["VC1","VC4","Coolant"])

    # Compare before / after isolating inner box
    # ax = data1[["VC1","VC4","BOX"]].plot(color=["black","red","green"],style="-",grid=True,title="",figsize=(10,7))
    # data4[["VC1","VC4","BOX"]].plot(color=["black","red","green"],style="--",grid=True,ax=ax) # Plot in the same figure
    # ax.set_xlabel("Time [h]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,2.6)
    # ax.legend(["VC (alu CB)","VC (copper CB)","Box","VC iso (alu CB)","VC iso (copper CB)","Box iso"])

    # Compare nitrogen flow with isolated inner box
    # ax = data4[["VC1","VC2","VC4"]].plot(color=["black","red","green"],style="-",grid=True,title="",figsize=(10,7))
    # data6[["VC1","VC2","VC4"]].plot(color=["black","red","green"],style="--",grid=True,ax=ax) # Plot in the same figure
    # ax.set_xlabel("Time [h]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,2.6)
    # ax.legend(["VC1 (6L/m)","VC2 (6L/m)","VC4 (6L/m)","VC1 (2L/m)","VC2 (2L/m)","VC4 (2L/m)"])

    # Compare Peltier voltages
    # ax = data2[["VC4"]].plot(grid=True,title="")
    # data1[["VC4"]].plot(grid=True,ax=ax) # Plot in the same figure
    # data3[["VC4"]].plot(grid=True,ax=ax) # Plot in the same figure
    # ax.set_xlabel("Time [h]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,2.6)
    # ax.legend(["VC4 (30V)","VC4 (25V)","VC4 (off)"])

    # Plot box component temperatures
    # ax = data1[["VC1","BOX","LID","COOL IN","COOL OUT","Meas T"]].plot(grid=True,title="")
    # ax.set_xlabel("Time [h]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,2.6)
    # ax.legend(["VC1","Box","Lid","Cool in","Cool out","Chiller"])

    # Plot comparison of VC1 and VC4
    # ax = data1[["VC1","VC4"]].plot(grid=True,title="")
    # ax.set_xlabel("Time [h]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,2.6)
    # ax.legend(["VC1","VC4"])

    # Plot difference before / after removing valves
    # ax = data3[["VC1"]].plot(grid=True,title="")
    # data5[["VC1"]].plot(grid=True,ax=ax) # Plot in the same figure
    # ax.set_xlabel("Time [h]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,7)
    # ax.legend(["VC1","VC1 (valves)"])

    # Plot cool in/out
    # ax = data6[["COOL IN","COOL OUT","Meas T"]].plot(grid=True,title="",figsize=(10,7))
    # ax.set_xlabel("Time [h]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,2.6)
    # ax.legend(["Cool In","Cool Out","Coolant"])

    # Plot VC4 and CB4
    # ax = data1[["VC4","CB4"]].plot(grid=True,title="")
    # ax.set_xlabel("Time [h]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,2.6)
    # ax.legend(["VC4","CB4"])

    # Plot VC4, CB4 and coolant before/after shortening tubes from chiller to box
    # ax = data6[["VC4","CB4","Meas T"]].plot(color=["black","red","green"],style="-",grid=True,title="")
    # data7[["VC4","CB4","Meas T"]].plot(color=["black","red","green"],style="--",grid=True,title="",ax=ax)
    # ax.set_xlabel("Time [h]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,2.6)
    # ax.legend(["VC4","CB4","Coolant","VC4 (short)","CB4 (short)","Coolant (short)"])

    # Plot VC1, CB1 and coolant before/after shortening tubes from chiller to box
    # ax = data6[["VC1","CB1","Meas T"]].plot(color=["black","red","green"],style="-",grid=True,title="")
    # data7[["VC1","CB1","Meas T"]].plot(color=["black","red","green"],style="--",grid=True,title="",ax=ax)
    # ax.set_xlabel("Time [h]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,2.6)
    # ax.legend(["VC1","CB1","Coolant","VC1 (short)","CB1 (short)","Coolant (short)"])



    # Plot VC4 after various optimisations
    # ax = data1["VC4"].plot(color="black",grid=True,title="")
    # data4["VC4"].plot(color="red",grid=True,title="",ax=ax)
    # data6["VC4"].plot(color="blue",grid=True,title="",ax=ax)
    # data7["VC4"].plot(color="green",grid=True,title="",ax=ax)
    # ax.set_xlabel("Time [h]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,2.6)
    # ax.legend(["VC4 (default)","VC4 (baseplate iso)","VC4 (N2 2L/min)","VC4 (short tubes)"])


    # Plot VC and CB in plastic box , chiller circulating at -20C, two peltier layers. Layer 1 at 25V, then layer 2 at 3,6,9,12,15,18 V
    # ax = data9[["COLDBLOCK","VACCHUCK"]].plot(color=["black","red"],grid=True,title="",figsize=(8,5))
    # ax.set_xlabel("Time [h]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,2.6)

    # Plot CB in plastic box, thermal shock at -25C at various pump speeds. No HV insert.
    # ax = data10["BOX"].plot(color="black",grid=True,title="",figsize=(8,5))
    # data11["BOX"].plot(color="red",grid=True,title="",ax=ax)
    # data12["BOX"].plot(color="green",grid=True,title="",ax=ax)
    # ax.set_xlabel("Time [min]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,12)
    # ax.legend(["Pump speed 5","Pump speed 3","Pump speed 1"])


    # Compare with/without new ground shim plate. Chiller + Peltiers at 25V starting at 20C
    # ax = data13[["COLDBLOCK","VACCHUCK"]].plot(color=["black","red"],style="-",grid=True,title="",figsize=(8,5))
    # data15[["COLDBLOCK","VACCHUCK"]].plot(color=["black","red"],style="--",grid=True,title="",ax=ax)
    # ax.set_xlabel("Time [min]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,65)
    # ax.legend(["Cold block (new ground shim)","Vacuum chuck (new ground shim)","Cold block (old ground shim)","Vacuum chuck (old ground shim)"])


    # Compare with/without new ground shim plate. Coolant held at -20C, then Peltiers on at 25V
    # ax = data14[["COLDBLOCK","VACCHUCK"]].plot(color=["black","red"],style="-",grid=True,title="",figsize=(8,5))
    # data16[["COLDBLOCK","VACCHUCK"]].plot(color=["black","red"],style="--",grid=True,title="",ax=ax)
    # ax.set_xlabel("Time [min]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,45)
    # ax.legend(["Cold block (new ground shim)","Vacuum chuck (new ground shim)","Cold block (old ground shim)","Vacuum chuck (old ground shim)"])


    # Plot heatup with Peltiers in reverse at 25V
    # ax = data17[["COLDBLOCK","VACCHUCK","Meas T"]].plot(color=["black","red","blue"],style="-",grid=True,title="",figsize=(8,5))
    # ax.set_xlabel("Time [min]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,12)
    # ax.legend(["Cold block","Vacuum chuck","Coolant"])


    # Compare cold jig cooldown with/without new ground shim plate + plate alone. Cooldown from +30C with Peltiers on at 25V
    # ax = data18["Meas T"].plot(color="blue",style="-",grid=True,title="",figsize=(8,5))
    # data19["Meas T"].plot(color="blue",style="--",grid=True,title="",ax=ax)
    # data20["Meas T"].plot(color="blue",style="-.",grid=True,title="",ax=ax)
    # data18["VC4"].plot(color="red",style="-",grid=True,title="",ax=ax)
    # data19["VC4"].plot(color="red",style="--",grid=True,title="",ax=ax)
    # data20["VC4"].plot(color="red",style="-.",grid=True,title="",ax=ax)
    # ax.set_xlabel("Time [min]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,165)
    # ax.legend(["Coolant (old ground shim)","Coolant (new ground shim)","Coolant (only plate)","VC4 (old ground shim)","VC4 (new ground shim)","VC4 (only alu plate)"])


    # Plot full cycle, coolant at -5C, +30C to -40C to +30C
    # ax = data21[["VC1","VC4","Meas T"]].plot(color=["black","red","blue"],style="-",grid=True,title="",figsize=(8,5))
    # ax.set_xlabel("Time [min]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,36.5)
    # ax.legend(["VC1","VC4","Coolant"])

    # Plot full cycle, coolant at -10C, +30C to -40C to +30C
    # ax = data23[["VC1","VC4","Meas T"]].plot(color=["black","red","blue"],style="-",grid=True,title="",figsize=(8,5))
    # ax.set_xlabel("Time [min]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,36.5)
    # ax.legend(["VC1","VC4","Coolant"])


    # Compare 16mm and 10mm tubes from chiller. Old VC4 ground shim. Cooldown +30C to -40C with Peltiers at 25V.
    # ax = data18["Meas T"].plot(color="blue",style="-",grid=True,title="",figsize=(8,5))
    # data22["Meas T"].plot(color="blue",style="--",grid=True,title="",ax=ax)
    # data18["CB4"].plot(color="black",style="-",grid=True,title="",ax=ax)
    # data22["CB4"].plot(color="black",style="--",grid=True,title="",ax=ax)
    # data18["VC4"].plot(color="red",style="-",grid=True,title="",ax=ax)
    # data22["VC4"].plot(color="red",style="--",grid=True,title="",ax=ax)
    # ax.set_xlabel("Time [min]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,140)
    # ax.legend(["Coolant (16mm pipes)","Coolant (10mm pipes)","CB4 (16mm pipes)","CB4 (10mm pipes)","VC4 (16mm pipes)","VC4 (10mm pipes)"])



    # Plot cold block thermal shocks at different pump speeds
    # ax = data24["COLDBLOCK"].plot(color="red",style="-",grid=True,title="",figsize=(8,5))
    # data25["COLDBLOCK"].plot(color="green",style="-",grid=True,title="",figsize=(8,5),ax=ax)
    # data26["COLDBLOCK"].plot(color="blue",style="-",grid=True,title="",figsize=(8,5),ax=ax)
    # data27["COLDBLOCK"].plot(color="orange",style="-",grid=True,title="",figsize=(8,5),ax=ax)    
    # data28["COLDBLOCK"].plot(color="purple",style="-",grid=True,title="",figsize=(8,5),ax=ax)
    # ax.set_xlabel("Time [min]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,5.5)
    # ax.legend(["Cold block (speed 1)","Cold block (speed 2)","Cold block (speed 3)","Cold block (speed 4)","Cold block (speed 5)"])



    # Compare fast cycle (25V cooldown, 25V heatup) vs. (30V-25V cooldown, 40V heatup)
    # ax = data23[["VC4","Meas T"]].plot(color=["red","blue"],style="-",grid=True,title="",figsize=(8,5))
    # data21[["VC4","Meas T"]].plot(color=["red","blue"],style="--",grid=True,title="",ax=ax)
    # ax.set_xlabel("Time [min]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,36.5)
    # ax.legend(["VC4 (-10C)","Coolant (-10C)","VC4 (-5C)","Coolant (-5C)"])



    # # Compare rapid Peltier cycling at different constant coolant temperatures
    # ax = data30["VACCHUCK"].plot(color="red",style="-",grid=True,title="",figsize=(8,5))
    # data31["VACCHUCK"].plot(color="blue",style="-",grid=True,title="",ax=ax)
    # data32["VACCHUCK"].plot(color="green",style="-",grid=True,title="",ax=ax)
    # data33["VACCHUCK"].plot(color="orange",style="-",grid=True,title="",ax=ax)
    # ax.set_xlabel("Time [min]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,27)
    # ax.legend(["VC (30V, coolant -25C, pump 5)","VC (30V, coolant -30C, pump 5)","VC (40V, coolant -30C, pump 5)","VC (30V, coolant -30C, pump 1)"])


    # Compare rapid Peltier cycling at 30V with/without graphite interface film
    # ax = data35["VACCHUCK"].plot(color="black",style="-",grid=True,title="Peltiers at 30V",figsize=(11,7))
    # data45["VACCHUCK"].plot(color="black",style="--",grid=True,title="",ax=ax)
    # data31["VACCHUCK"].plot(color="red",style="-",grid=True,title="",ax=ax)
    # data37["VACCHUCK"].plot(color="red",style="--",grid=True,title="",ax=ax)
    # data39["VACCHUCK"].plot(color="red",style=":",grid=True,title="",ax=ax)
    # data44["VACCHUCK"].plot(color="blue",style="--",grid=True,title="",ax=ax)
    # ax.set_xlabel("Time [min]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,42)
    # ax.legend(["VC new (paste)","VC new (graphite)","VC old (paste)","VC old (graphite)","VC old (no top graphite)","GP (graphite)"])


    # Compare rapid Peltier cycling at 40V with/without graphite interface film
    # ax = data32["VACCHUCK"].plot(color="black",style="-",grid=True,title="Peltiers at 40V",figsize=(9,6))
    # data36["VACCHUCK"].plot(color="red",style="-",grid=True,title="",ax=ax)
    # data40["VACCHUCK"].plot(color="blue",style="-",grid=True,title="",ax=ax)
    # ax.set_xlabel("Time [min]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,20)
    # ax.legend(["VC old (paste)","VC old (graphite)","VC old (no top graphite)"])



    # Compare thermal cycling speed with/without chiller cycling
    # ax = data35["VACCHUCK"].plot(color="red",style="-",grid=True,title="Peltiers at 30V",figsize=(11,7))
    # data19["VC4"].plot(color="black",style="-",grid=True,title="",ax=ax)
    # ax.set_xlabel("Time [min]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,80)
    # ax.legend(["VC (chiller temp. constant)","VC (chiller temp. cycling)"])

    # Two layers of Peltiers, cycle from +60C to -55C 
    # ax = data47["Chan 1"].plot(color="red",style="-",grid=True,figsize=(7,5))
    # ax.set_xlabel("Time [min]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,40)
    # ax.legend(["VC"])




    # Compare Peltier cycling of one chill stack in 4-module jig using Huber chiller
    # ax = data48["VACCHUCK"].plot(color="black",style="-",grid=True,figsize=(9,6))
    # data47["Chan 1"].plot(color="red",style="-",grid=True,title="",ax=ax)
    # data49["VC4"].plot(color="green",style="-",grid=True,title="",ax=ax)
    # data50["VC4"].plot(color="blue",style="-",grid=True,title="",ax=ax)
    # data51["VC4"].plot(color="orange",style="-",grid=True,title="",ax=ax)
    # #data52["VC4"].plot(color="teal",style="-",grid=True,title="",ax=ax)
    # ax.set_xlabel("Time [min]")
    # ax.set_ylabel("Temperature [C]")
    # ax.set_xlim(0,55)
    # ax.xaxis.set_ticks([x for x in range(0,60,5)])
    # ax.yaxis.set_ticks([y for y in range(-60,65,5)])
    # ax.legend(["VC (19V/7.0V, Grant -20C, plastic box)","VC (19V/7.0V, Huber -20C, plastic box)","VC (19V/7.0V, Huber -20C, 4-jig)","VC (19V/7.0V, Huber -20C, 4-jig + insulation)","VC (18V/6.6V, Huber -20C, 4-jig + insulation)"]) #,"VC (18V/6.6V, Huber -25C, 4-jig + insulation"])


    # Plot cycling of one chill stack in 4-module jig using Huber chiller, show VC and CB temperatures
    ax = data51["VC4"].plot(color="red",style="-",grid=True,title="")
    data51["CB4"].plot(color="blue",style="-",ax=ax)
    ax.set_xlabel("Time [min]")
    ax.set_ylabel("Temperature [C]")
    ax.set_xlim(0,40)
    ax.legend(["VC","CB"])






    plt.show()