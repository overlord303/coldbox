from chillerModel import *
import csv
from array import array
from ROOT import *
import pandas as pd
import matplotlib.pyplot as plt

def loadData(filename) :

    print "processing", filename
    
    # Load CSV file
    data = pd.read_csv(filename,skiprows=2,
                   parse_dates=True,
                   dayfirst=True,
                   infer_datetime_format=True,
                   index_col=0)

    # Find columns with raw values
    rawValues = [colname
                     for colname in data.columns.values
                     if colname.find("_raw") != -1]

    # Find columns called 'Col 1', 'Col 2', etc
    colValues = [ colname
                      for colname in data.columns.values
                      if colname.find("Col ") != -1]

    # Find columns that are unnamed
    unnameValues =  [ colname
                      for colname in data.columns.values
                      if colname.find("Unnamed: ") != -1]
    drop_columns = rawValues + colValues + unnameValues

    # Drop values in drop_columns
    data.drop(drop_columns, axis=1, inplace=True)

    # Find first time Set T goes to -30
    # tZero = (data[data["Set T"] < 0.0])["Elapsed"].iloc[0]

    # Now make new Elapsed time that uses tZero to define 0.0 time and
    # have time in hours
    #data["Elapsed_hr"] = (data["Elapsed"] - tZero) / 3600.0
    
    return data



#############################################
# Config
#############################################

# "Log170821202145.csv":
# Heatup without chiller 


# "Log170607152428.csv":
# VC2/CB2 : Snow Chuck + Peltier
# VC3/CB3 : Snow Chuck + No Peltier
# CB4/VC4 : Yu Chuck + Peltier 



duration = 3. # hours
i_data = 0

data = [None]*100
data[0] = loadData("Log171124145421.csv") # 30 --> -40C and Peltiers at 25V
data[1] = loadData("Log171205133749.csv") # 30 --> -40C and Peltiers at 30V
data[2] = loadData("Log171128153545.csv") # 30 --> -40C and Peltiers off
data[3] = loadData("Log171129151835.csv") # 30 --> -40C and Peltiers at 25V and base plates isolated from inner box
data[4] = loadData("Log171003085924.csv") # 30 --> -40C and Peltiers off and valves in place
data[5] = loadData("Log170915100306.csv") # 30 --> -40C with two bath switching and peltiers off and bulkheads removed, valves in place
data[6] = loadData("Log171214111206.csv") # 30 --> -40V and peltiers at 25V and pase plates isolation from inner box + N2 flow at 2L/min
# data[0] to [3] are without valves

#print data[2].columns.values

data = [d for d in data if d is not None]
t_n2 = [-17.]*len(data)
t_lab = [22.]*len(data)
t_start = [None]*len(data)
t_coolstart = [None]*len(data)
data_t0 = [0.]*len(data)

data_t0[0] = 20.
data_t0[1] = 0.
data_t0[2] = 0.
data_t0[3] = 0.
data_t0[4] = 0.
data_t0[5] = 246.
data_t0[6] = 0.


for i in range(len(data)):

	# Change time to hours and replace index in dataframe 
	data[i]["Elapsed_hr"] = (data[i]["Elapsed"] - data_t0[i]) / 3600.0
	data[i].reset_index(inplace=True)
	data[i].set_index(data[i]["Elapsed_hr"], inplace=True)

	# Check if n2 and lab temperatures are available in dataframe 
	try:
		t_n2[i] = data[i]["ENV TEMP"].values.tolist()
		print "Data[{}] has N2 temp".format(i)
	except KeyError:
		continue
	try:
		t_lab[i] = data[i]["LAB"].values.tolist()
		print "t_lab[{}] has lab temp".format(i)
	except:
		continue

	# Get starting temperatures
	t_start[i] = data[i]["CB3"].values[0]
	t_coolstart[i] = data[i]["Meas T"].values[0]


#--------------------------------------------------------------------------------------------


# Old chill stack mass [kg]
# m_bp = 4*0.320831860888 # base plate
# m_cb = 4*0.628743896627 # cold block
# m_vc = 4*1.37407699333 # vacuum chuck

# New chill stack mass [kg]
m_bp = 4*1.059 # base plate
m_cb = 4*1.497 # cold block
m_vc = 4*1.157*0.45 # vacuum chuck

m_sb = 0.0011798 * 8030.0 # steel box
m_sl = 0.00053 * 8030.0 # steel lid
m_pi = 1. #4.08576 # copper pipes

V_cool = 10. # Volume of coolant in litres

# Specific heat capacities [J/(kg*Kelvin)]
shc_steel = 502.416
shc_al = 902.
shc_cu = 385.
shc_brass = 380.

# Old chill stack alpha
# alpha_bp = m_bp * shc_al
# alpha_cb = m_cb * shc_al
# alpha_vc = m_vc * shc_al

# New chill stack alpha
alpha_bp = m_bp * shc_brass
alpha_cb = m_cb * shc_cu
alpha_vc = m_vc * shc_al

alpha_sb = m_sb * shc_steel
alpha_sl = m_sl * shc_steel
alpha_pi = m_pi * shc_cu


simulation = coldboxsim()

# Start conditions
simulation.setChiller("grant")
simulation.setDuration(duration*3600) # seconds
simulation.setChill(True)
simulation.setTempStart(t_start[i_data])
simulation.setTempEnv(t_lab[i_data])
simulation.setTempN2(t_n2[i_data])
simulation.setTempCoolantStart(t_coolstart[i_data])

# Heat capacities
simulation.setHeatCapacityBP(alpha_bp)
simulation.setHeatCapacityCB(alpha_cb)
simulation.setHeatCapacityVC(alpha_vc)
simulation.setHeatCapacitySB(alpha_sb)
simulation.setHeatCapacitySL(alpha_sl)
simulation.setHeatCapacityPI(alpha_pi)
simulation.setVolumeCoolant(V_cool)

# Heat transfer [W/K]
simulation.setConductanceBox(0.41)
simulation.setConductanceTubes(0.30)
simulation.setHeatTransferCB_PI(5.)
simulation.setHeatTransferCB_SB(10.)
simulation.setHeatTransferPI_CO(20.)
simulation.setHeatTransferPI_SB(0.)
simulation.setHeatTransferPI_N2(0.2)
simulation.setHeatTransferSB_SL(0.2)
simulation.setHeatTransferSB_N2(0.7)
simulation.setHeatTransferVC_N2(2.0)

# Aluminium CB without peltiers
simulation.setHeatTransferCB_CO(34.)
simulation.setHeatTransferCB_VC(0.25)

# Copper CB with peltiers
# simulation.setHeatTransferCB_CO(3*34.)
# simulation.setHeatTransferCB_VC(10.)

# Peltier settings
simulation.setPeltierOn(True)
#simulation.setPeltierPower(4*37.5)
#simulation.setPeltierEfficiency(0.7)


# Current box
output = simulation.runSim()

simulation.setChiller("huber")

output2 = simulation.runSim()

# Plot data
#ax = data[i_data][["CB4","VC4","Meas T"]].plot(color=["black","red","blue","gray"],style="-",grid=True,title="")
#output[["CB","VC","Meas T"]].plot(color=["black","red","blue","gray"],style="--",grid=True,title="",ax=ax)
# ax.set_xlabel("time [h]")
# ax.set_ylabel("Temperature [C]")
# ax.set_xlim(0,duration)
# ax.legend(["CB4 (data)","VC4 (data)","Coolant (data)","CB (sim)","VC (sim)","Coolant (sim)"])

ax = output[["CB","VC","Meas T"]].plot(color=["black","red","blue","gray"],style="-",grid=True,title="")
output2[["CB","VC","Meas T"]].plot(color=["black","red","blue","gray"],style="--",grid=True,title="",ax=ax)
ax.set_xlabel("time [h]")
ax.set_ylabel("Temperature [C]")
ax.set_xlim(0,duration)
ax.legend(["CB4 (Grant)","VC4 (Grant)","Coolant (Grant)","CB (Huber)","VC (Huber)","Coolant (Huber)"])







# No vacuum chucks
# simulation.setHeatCapacityVC(0.)
# time2,tempvecCoolant2,tempvecCB2,tempvecVC2,tempvecSteelbox2,tempvecCopper2,tempvecSteellid2 = simulation.runSim()

# No steel box
# simulation.setHeatCapacityVC(alpha_vc)
# simulation.setHeatCapacitySB(1.)
# simulation.setHeatCapacitySL(0.)
# time3,tempvecCoolant3,tempvecCB3,tempvecVC3,tempvecSteelbox3,tempvecCopper3,tempvecSteellid3 = simulation.runSim()

# No vacuum chucks or steel box
# simulation.setHeatCapacityVC(0.)
# time4,tempvecCoolant4,tempvecCB4,tempvecVC4,tempvecSteelbox4,tempvecCopper4,tempvecSteellid4 = simulation.runSim()


# time = [(t+time_offset*60.)/(60.*60.) for t in time]
# # time2 = [(t+time_offset*60.)/(60.*60.) for t in time2]
# # time3 = [(t+time_offset*60.)/(60.*60.) for t in time3]
# # time4 = [(t+time_offset*60.)/(60.*60.) for t in time4]
# time_meas = [t/(60.*60.) for t in time_meas]

plt.show()

raw_input("...")


# Dictionary:
# BP = base plate
# CB = cold blocks
# VC = vacuum chucks
# SB = steel box
# SL = steel lid
# PI = pipes
# CO = coolant