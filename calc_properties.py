import math

def Vhole(radius,depth):
 	return math.pi*(radius*radius)*depth

#####################################
# Volumes of components
#####################################

# Volume of base plate
V_box = 200*100*6
V_hole1 = Vhole(2.5,6)
V_hole2 = Vhole(2.5,2) + Vhole(5,4)
V_hole3 = Vhole(1.75,6)
V_baseplate = V_box - 2*V_hole1 - 2*V_hole2 - 4*V_hole3
V_baseplate /= 1000**3
#print "V_baseplate =", V_baseplate, "m3"

#-----------------

# Volume of cold block
V_box = 100*100*25
V_hole1 = Vhole(3.5,5) + Vhole(2,20)
V_tube = Vhole(4,83)
V_coldblock = V_box - V_hole1 - 4*V_tube
V_coldblock /= 1000**3
#print "V_coldblock =", V_coldblock, "m3"

#-----------------

# Volume of vacuum chuck
V_box = 170*140*20.1
V_elevations = 87*87*1.9 + 100*100*3
V_hole1 = Vhole(5,9) + Vhole(8,5) + Vhole(2.5,20.1-9-5)
V_hole2 = Vhole(1.5,20.1)
V_hole3 = Vhole(3.5,100) + Vhole(3.175,12)
V_hole4 = Vhole(3.5,130) + Vhole(3.175,12)
V_vacuumchuck = V_box + V_elevations - 2*V_hole1 - 4*V_hole2 - V_hole3 - V_hole4
V_vacuumchuck /= 1000**3
#print "V_vaccumchuck =", V_vacuumchuck, "m3"

#-----------------

#Inner box taken from Solidworks
V_innerbox = 0.0011798
V_lid = 0.00053
print "V_innerbox =", V_innerbox, "m3"
print "V_lid =", V_lid, "m3"

#Copper pipes taken from Solidworks
V_copperpipes = 0.000456
print "V_copperpipes =", V_copperpipes, "m3"


#####################################
# Masses of components
#####################################

# Densities [kg/m3]
dens_al = 2700.
dens_cu = 8960.
dens_steel = 8030.

m_baseplate = V_baseplate * dens_al
m_coldblock = V_coldblock * dens_al
m_vacuumchuck = V_vacuumchuck*dens_al
m_innerbox = V_innerbox * dens_steel
m_al = (4*V_baseplate + 4*V_coldblock + 4*V_vacuumchuck + V_innerbox) * dens_al
m_cu = V_copperpipes * dens_cu
m_stacks = (4*V_baseplate + 4*V_coldblock + 4*V_vacuumchuck) * dens_al
m_coolant = 12.*(0.6*1.11 + 0.4*1.0)

m_newbaseplate = 1.059
m_newcoldblock = 1.497
m_newvacuumchuck = 1.157


#print "Mass of one base plate =", m_baseplate
#print "Mass of one cold block =", m_coldblock
#print "Mass of one vacuum chuck =", m_vacuumchuck
#print "Mass of aluminium box =", m_innerbox
#print "Total mass of four chill stacks =", m_stacks
#print "Total mass of aluminium =", m_al, "kg"
#print "Total mass of copper =", m_cu, "kg"

#####################################
# Calculate total heat capacity
#####################################

# Specific heat capacities [J/(kg*Kelvin)]
shc_al = 902.
shc_cu = 385.
shc_steel = 502.416
shc_brass = 380.
shc_coolant = 3200.

alpha_oldbaseplate = m_baseplate*shc_al
alpha_oldcoldblock = m_coldblock*shc_al
alpha_oldvacuumchuck = m_vacuumchuck*shc_al
alpha_newbaseplate = m_newbaseplate*shc_brass
alpha_newcoldblock = m_newcoldblock*shc_cu
alpha_newvacuumchuck = m_newvacuumchuck*shc_al
alpha_steelbox = m_innerbox*shc_steel
alpha_copperpipes = m_cu*shc_cu
alpha_coolant = m_coolant*shc_coolant

print "Heat capacity of old base plate:", alpha_oldbaseplate, "J/K"
print "Heat capacity of old cold block:", alpha_oldcoldblock, "J/K"
print "Heat capacity of old vacuum chuck:", alpha_oldvacuumchuck, "J/K"
print "------------------------------------------------------------"
print "Heat capacity of new base plate:", alpha_newbaseplate, "J/K"
print "Heat capacity of new cold block:", alpha_newcoldblock, "J/K"
print "Heat capacity of new vacuum chuck:", alpha_newvacuumchuck, "J/K"
print "------------------------------------------------------------"
print "Heat capacity of steel box:", alpha_steelbox, "J/K"
print "Heat capacity of copper pipes:", alpha_copperpipes, "J/K"
print "Heat capacity of coolant:", alpha_coolant, "J/K"


print "Heat capacity of one old chill stack:", alpha_oldbaseplate+alpha_oldcoldblock+alpha_oldvacuumchuck, "J/K"
print "Heat capacity of one new chill stack:", alpha_newbaseplate+alpha_newcoldblock+alpha_newvacuumchuck, "J/K"
print "Total heat capacity with old chill stacks:", 4*(alpha_oldbaseplate+alpha_oldcoldblock+alpha_oldvacuumchuck) + alpha_steelbox + alpha_copperpipes, "J/K"
print "Total heat capacity with new chill stacks:", 4*(alpha_newbaseplate+alpha_newcoldblock+alpha_newvacuumchuck) + alpha_steelbox + alpha_copperpipes, "J/K"



#####################################
# Insulation properties
#####################################

# Insulation dimensions:
# -------------------------
# Outer dimensions:
# LxWxH = 1150 x 650 x 300 mm
# Area: 2.575 m2

# Inner dimensions:
# LxWxH = 950 x 450 x 100 mm
# Area: 1.135 m2

# In-between dimensions:
# LxWxH = 1050 x 550 x 200 mm
# Area: 1.795 m2

print "Outer area of insulation =", 2.575, "m2"
print "Inner area of insulation =", 1.135, "m2"
print "Average of inner and outer area of insulation =", (2.575+1.135)/2., "m2"

# C = conductance [W/K] : Heat passing through an object with specific area A and thickness L
# k = Thermal conductivity [W/(m*K)]
# C = k*A/L
# Conductance of insulation

k_ins = 0.022
A_ins = 1.855 #[m^2]
L_ins = 0.1 #[m]
C_ins = k_ins*A_ins/L_ins

print "Conductance of insulation =", C_ins, "W/K"